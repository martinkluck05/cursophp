<?php 

	function negrita($palabra)
	{
		echo "<b>".$palabra."</b>";
	}
	
?>
 
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Funciones</title>
</head>
<body>
	<form action="funciones.php" method="post">
		<input type="text" name="palabra" value="Palabra" id="palabra"/>
		<input type="submit" name="enviar" value="Enviar" id="enviar"/>
	</form>	
	<br />
</body>
</html>

<?php 
	$palabra = $_POST['palabra'];
	negrita($palabra);
 ?>
 <br />
 <?php 
 
 	function duplicar($numero){
 		return $numero*2;
 	}
 
  ?>
  

<?php 

	echo duplicar(2);

 ?>
 
<?php 

		function multiplicar($numero,$multi=1){
			echo $numero*$multi;
		}
		
		multiplicar(3, 4);
 ?>
 

<?php 

	negrita(duplicar(2));
	
	function cursiva($frase){
		return "<i>".$frase."</i><br>";
	}
	function triplicar($numero){
		return $numero*3;
	}


	echo cursiva(triplicar(20948));
 ?>
 
 
 <!doctype html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8" />
 	<title>Moneda</title>
 </head>
 <body>
 	<form action="funciones.php" method="get" accept-charset="utf-8">
	   		<input type="text" name="monto" value="" id="monto"/>
	   		<select name="moneda" id="moneda">
	   			<option value="d">Dolar</option>
	   			<option value="e">Euro</option>
	   			<option value="r">Real</option>
	   		</select>
			 <p><input type="submit" value="Enviar"/></p>
			 <br />
	 </form>
 </body>
 </html>
 
<?php 
	function convercion($monto,$moneda="d"){
		switch ($moneda) {
			case 'd':
				return $monto * 9.44;
				break;
			case 'e':
				return $monto * 11.44;
				break;
			case 'r':
				return $monto * 3.13;
				break;
			default:
				return $monto;
				break;
		}
	}
	
	echo round(convercion($_GET['monto'], $_GET['moneda']),2);
	

 ?>