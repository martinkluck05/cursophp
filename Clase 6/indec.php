<?php
		$meses = array("enero", "febrero", "marzo", "abril", 
						"mayo", "junio", "julio", "agosto", 
						"septiembre", "octubre", "noviembre", "diciembre");
		$inflacion = array(
						3.2, 2.5, 3.6, 2.1,
						2.0, 2.1, 3.1, 3.5,
						2.3, 1.9, 2.4, 2.9);

	/* 
		informar:   mes  valor 
		averiguar: 
					valor mayor
					valor menor
					promedio
					total
	*/
    $cont = count($meses);
    $total = 0;
    $prom = 0;
?>	

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Indec</title>
    <link rel="stylesheet" href="style.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
   <div class="container">
    <table class="table table-bordered" style="text-align:center;">
        <thead>
            <tr style="color: white; background-color: black;">
                <th><h1 style="text-align:center;">Mes</h1></th>
                <th><h1 style="text-align:center;">Valor</h1></th>
            </tr>
        </thead>
        <tbody>
        <?php for($i=0;$i<$cont;$i++){?>
        <tr>
            <td><?php echo $meses[$i]; ?></td>
            <td><?php echo $inflacion[$i]; ?></td>
        </tr>
        <?php 
             //$total += $inflacion[$i];
            }  
            $total = array_sum($inflacion); ?>
        </tbody>
        <tfoot>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr style="color:white; background-color: blue;">
                <td>Maximo: </td>
                <td><?php echo max($inflacion); ?></td>
            </tr>
            <tr style="color:white; background-color: blue;">
                <td>Minimo: </td>
                <td><?php echo min($inflacion); ?></td>
            </tr>
            <tr style="color:white; background-color: blue;">
                <td>Total: </td>
                <td><?php echo $total; ?></td>
            </tr>
            <tr style="color:white; background-color: blue;">
                <td>Promedio: </td>
                <td><?php 
                    $prom = round($total/$cont,2);    
                    echo $prom; ?></td>
            </tr>
        </tfoot>
    </table>
    </div>
</body>
</html>					