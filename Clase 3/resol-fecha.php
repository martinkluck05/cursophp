<?php 
	date_default_timezone_set("America/Argentina/Buenos_Aires");

	$nSemana = date("w");
	switch ($nSemana) {
		case 0:
			$semana = "Domingo";
			break;
		
		case 1:
			$semana = "Lunes";
			break;
		
		case 2:
			$semana = "Martes";
			break;
		
		case 3:
			$semana = "Miércoles";
			break;
		
		case 4:
			$semana = "Jueves";
			break;
		
		case 5:
			$semana = "Viernes";
			break;
		
		default:
			$semana = "Sábado";
			break;
	}

	$diaMes = date("j");

	$nMes = date("n");
	// switch para los meses
	$anio = date("Y");

	echo "Hoy es ", $semana, " ", $diaMes, " de "
?>