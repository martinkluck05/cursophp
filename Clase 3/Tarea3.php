<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tarea 3</title>
</head>
<body>
	<?php 
		date_default_timezone_set("America/Argentina/Buenos_Aires");
		$nSemana = date("w");
		switch ($nSemana) {
			case 0:
				$semana = "Domingo";
				break;
			case 1:
				$semana = "Lunes";
				break;
			case 2:
				$semana = "Martes";
				break;
			case 3:
				$semana = "Miercoles";
				break;
			case 4:
				$semana = "Jueves";
				break;
			case 5:
				$semana = "Viernes";
				break;
			case 6:
				$semana = "Sabado";
				break;
			default:
				$semana = "Sabado";
				break;
		}

		$diaMes = date("j");

		$nMes = date("n");

		switch ($nMes) {
			case 0:
				$mes = "Enero";
				break;
			case 1:
				$mes = "Febrero";
				break;
			case 2:
				$mes = "Marzo";
				break;
			case 3:
				$mes = "Abril";
				break;
			case 4:
				$mes = "Mayo";
				break;
			case 5:
				$mes = "Junio";
				break;
			case 6:
				$mes = "Julio";
				break;
			case 7:
				$mes = "Agosto";
				break;
			case 8:
				$mes = "Septiembre";
				break;
			case 9:
				$mes = "Octubre";
				break;
			case 10:
				$mes = "Noviembre";
				break;
			default:
				$mes = "Diciembre";
				break;
		}

		$anio = date("Y");

		echo "Hoy es ",$semana," ",$diaMes," de ", $mes, " del ",$anio;
	 ?>
</body>
</html>