<?php
	$prd_id = $_GET['prd_id'];
	$titulo = "Editar - Proyecto integrador";
	include 'validar.php';
	require "conexion.php";
	$sql = "SELECT cat_id, cat_nombre FROM categorias";
	$sqlP = "SELECT prd_id,prd_nombre, prd_descripcion, prd_precio, prd_foto1, prd_foto2, cat_id FROM productos WHERE prd_id=".$prd_id;
									
	$resultado = mysqli_query($link, $sql) or die(mysqli_error($link));
	$resultadoP = mysqli_query($link, $sqlP) or die(mysqli_error($link));
	$filaP = mysqli_fetch_assoc($resultadoP);
	
	function categoria($fila, $value){
		if($fila == $value){
			echo "selected";
		}
	}
?>
<?php include "encabezado.php"; ?>
</head>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="nav">
		<?php  include "menu.php"; ?>
	</div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		
		<form action="editar-producto.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
		  			<table id="paneles">
		  				<tr>
		  					<td>Nombre</td>
		  					<td><input type="text" name="prd_nombre" value="<?php echo $filaP['prd_nombre']; ?>" id="prd_nombre"/></td>
		  				</tr>
		  				<tr>
		  					<td>Descripcion</td>
		  					<td><textarea name="prd_descripcion" rows="8" cols="40" id="prd_descripcion"><?php echo $filaP['prd_descripcion']; ?></textarea></td>
		  				</tr>
		  				<tr>
		  					<td>Precio</td>
		  					<td><input type="text" name="prd_precio" value="<?php echo $filaP['prd_precio']; ?>" id="prd_precio"/></td>
		  				</tr>
		  				<tr>
		  					<td>Categoria</td>
		  					<td>
								<select name="cat_id" id="cat_id">
									<?php
									 	while($fila = mysqli_fetch_assoc($resultado)){ ?>
									 	<option value="<?php echo $fila['cat_id']; ?>" <?php categoria($filaP['cat_id'], $fila['cat_id']); ?>><?php echo $fila['cat_nombre']; ?></option>
									<?php } ?>
								</select>
		  					</td>
		  				</tr>
		  				<tr>
		  					<td>Imagen Miniatura</td>
		  					<td>
		  						<img src="imagenes/<?php echo $filaP['prd_foto1']; ?>" alt="" />
		  						<input type="file" name="prd_foto2" value="" id="prd_foto2"/>
		  					</td>
		  				</tr>
		  				<tr>
		  					<td>Imagen Ampliada</td>
		  					<td>
		  						<img src="imagenes/<?php echo $filaP['prd_nombre']; ?>" alt="" />
		  						<input type="file" name="prd_foto1" value="" id="prd_foto1"/></td>
		  				</tr>
		  				<tr>
		  					<td colspan="2" class="centrar">
		  						<input type="hidden" name="prd_id" value="<?php echo $filaP['prd_id']; ?>" id="prd_id"/>
		  						<input type="submit" name="enviar" value="Modificar producto" id="enviar"/>
		  					</td>
		  				</tr>
		  			</table>
		</form>
		
	</div>
	<div id="pie">
		<?php  include "pie.php"  ?>
	</div>
	
</body>
</html>