<?php
	$titulo = "Panel de control - Proyecto integrador";;
?>
<?php include "encabezado.php"; ?>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="nav">
		<?php  include "menu.php"; ?>
	</div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		<table id="panel">
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th colspan="2"><a href="form-alta-categoria.php"><img src="imgs/add.png" alt="" title="Agregar Producto"/></a></th>
			</tr>
			<!--Dinamico-->
			<?php
				require "conexion.php";
				$sql = "SELECT cat_id, cat_nombre FROM categorias";
				
				$resultado = mysqli_query($link, $sql) or die(mysqli_error($link)); 
				$cantidad = mysqli_num_rows($resultado);
			 ?>
			 <?php
			 	$cont=0;
			 	while($fila = mysqli_fetch_assoc($resultado)){
			  
			  ?>
			<tr>
				<td class="lista"><?php echo $fila['cat_id']; ?></td>
				<td class="lista"><?php echo $fila['cat_nombre']; ?></td>
				<td class="class"><img src="imgs/editar3.png" alt="" title="Editar"/><a href="form-borrar-categoria.php?cat_id=<?php echo $fila['cat_id']; ?>"><img src="imgs/Trash.png" alt="" title="Eliminar"/></a></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="5" class="pie">
					Se han encontrado <?php echo $cantidad; ?> registros.
				</td>
			</tr>
		</table>
		
		
	</div>
	<div id="pie">
		<?php  include "pie.php"  ?>
	</div>
	
</body>
</html>