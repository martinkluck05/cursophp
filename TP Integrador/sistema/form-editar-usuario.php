<?php
	$usu_id = $_GET['usu_id'];
	$titulo = "Editar - Proyecto integrador";
	require "conexion.php";
	$sql = "SELECT usu_id, usu_login, usu_clave, usu_nombre, usu_email FROM usuarios WHERE usu_id=".$usu_id;
									
	$resultado = mysqli_query($link, $sql) or die(mysqli_error($link));
	$fila = mysqli_fetch_assoc($resultado);
	
?>
<?php include "encabezado.php"; ?>
</head>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="nav">
		<?php  include "menu.php"; ?>
	</div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		
		<form action="editar-usuario.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
		  			<table id="paneles">
		  				<tr>
		  					<td>Login</td>
		  					<td><input type="text" name="usu_login" value="<?php echo $fila['usu_login']; ?>" id="usu_login"/></td>
		  				</tr>
		  				<tr>
		  					<td>Clave</td>
		  					<td><input type="text" name="usu_clave" value="<?php echo $fila['usu_clave']; ?>" id="usu_clave"/></td>
		  				</tr>
		  				<tr>
		  					<td>Nombre</td>
		  					<td><input type="text" name="usu_nombre" value="<?php echo $fila['usu_nombre']; ?>" id="usu_nombre"/></td>
		  				</tr>
		  				<tr>
		  					<td>E-Mail</td>
		  					<td><input type="text" name="usu_email" value="<?php echo $fila['usu_email']; ?>" id="usu_email"/></td>
		  				</tr>
		  				<tr>
		  					<td colspan="2" class="centrar">
		  						<input type="hidden" name="usu_id" value="<?php echo $fila['usu_id']; ?>" id="usu_id"/>
		  						<input type="submit" name="enviar" value="Modificar usuario" id="enviar"/>
		  					</td>
		  				</tr>
		  			</table>
		</form>
		
	</div>
	<div id="pie">
		<?php  include "pie.php"  ?>
	</div>
	
</body>
</html>