function confirmacion(){
	var mensaje = 'Si pulsa el boton "Aceptar", se eliminara el producto seleccionado.';
	
	if (confirm(mensaje)) {
		return true;
	}
	//redireccion a panel-productos
	window.location='panel-productos.php';
	return false;
}

function confCat(){
	var mensaje = 'Si pulsa el boton "Aceptar", se eliminara la categoria seleccionada.';
	
	if (confirm(mensaje)) {
		return true;
	}
	//redireccion a panel-productos
	window.location='panel-categorias.php';
	return false;
}