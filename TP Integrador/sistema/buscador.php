<?php
	$titulo = "Buscador de Productos";;
?>
<?php include "encabezado.php"; ?>
</head>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="nav">
		<?php  include "menu.php"; ?>
	</div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		<form action="resultado.php" method="post" accept-charset="utf-8">
			<table class="paneles">
				<tr>
					<td>Ingrese criterio de busqueda</td>
					<td>
						<input type="text" name="busqueda" value="" id="busqueda"/>
					</td>
				</tr>
				<tr>
					<td>Categoria:</td>
					<td>
						<?php
							require "conexion.php";
							$sql = "SELECT cat_id, cat_nombre FROM categorias";
							
							$resultado = mysqli_query($link, $sql) or die(mysqli_error($link));
						 ?>
						<select name="categorias" id="categorias">
							<?php
							 	$cont=0;
							 	while($fila = mysqli_fetch_assoc($resultado)){ ?>
							 	<option value="<?php echo $fila['cat_id']; ?>"><?php echo $fila['cat_nombre']; ?></option>
							<?php } ?>
							<option value="0">Todas</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="centrar">
						<input type="submit" value="Buscar"/>
					</td>
				</tr>
			</table>
		</form>
		
		
	</div>
	<div id="pie">
		<?php  include "pie.php"  ?>
	</div>
	
</body>
</html>