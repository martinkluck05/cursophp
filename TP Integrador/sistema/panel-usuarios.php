<?php
	$titulo = "Panel de usuarios - Proyecto integrador";;
?>
<?php include "encabezado.php"; ?>
</head>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="nav">
		<?php  include "menu.php"; ?>
	</div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		<table id="panel">
			<tr>
				<th>ID</th>
				<th>Login</th>
				<th>Clave</th>
				<th>Nombre</th>
				<th>E-Mail</th>
				<th colspan="2"><a href="form-alta-usuario.php"><img src="imgs/add.png" alt="" title="Agregar Usuario"/></a></th>
			</tr>
			<!--Dinamico-->
			<?php
				require "conexion.php";
				$sql = "SELECT usu_id, usu_login,usu_clave,usu_nombre,usu_email FROM usuarios";
				
				$resultado = mysqli_query($link, $sql) or die(mysqli_error($link)); 
				$cantidad = mysqli_num_rows($resultado);
			 ?>
			 <?php
			 	$cont=0;
			 	while($fila = mysqli_fetch_assoc($resultado)){
			  
			  ?>
			<tr>
				<td class="lista"><?php echo $fila['usu_id']; ?></td>
				<td class="lista"><?php echo $fila['usu_login']; ?></td>
				<td class="lista"><?php echo $fila['usu_clave']; ?></td>
				<td class="lista"><?php echo $fila['usu_nombre']; ?></td>
				<td class="lista"><?php echo $fila['usu_email']; ?></td>
				<td class="class">
					<a href="form-editar-usuario.php?usu_id=<?php echo $fila['usu_id']; ?>">
						<img src="imgs/editar3.png" alt="" title="Editar"/>
					</a>
					<a href="form-borrar-usuario.php?usu_id=<?php echo $fila['usu_id']; ?>">
						<img src="imgs/Trash.png" alt="" title="Eliminar"/>
					</a>
				</td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="6" class="pie">
					Se han encontrado <?php echo $cantidad; ?> registros.
				</td>
			</tr>
		</table>
		
		
	</div>
	<div id="pie">
		<?php  include "pie.php"  ?>
	</div>
	
</body>
</html>