<?php
	$titulo = "Borrar - Proyecto integrador";
	include 'validar.php';
?>
<?php include "encabezado.php"; ?>
</head>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="nav">
		<?php  include "menu.php"; ?>
	</div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		
		
<div class="container">
<?php 
$prd_id = $_GET['prd_id'];
require "conexion.php";
$sql = "SELECT prd_nombre, prd_descripcion, prd_precio, prd_foto1
		FROM productos 
		WHERE prd_id=".$prd_id;
$resultado = mysqli_query($link, $sql) or die(mysqli_error($link)); 
$fila = mysqli_fetch_assoc($resultado);
 ?>
<form action="borrar-producto.php" method="get" onsubmit="return confirmacion()">
<div id="name">
				  <table>
				  	<th colspan="2"><h2>Se eliminara el siguiente producto:</h2></th>
				  	<tr>
				  		<td style="width: 25%">Nombre</td>
				  		<td style="width: 75%"><?php echo  $fila['prd_nombre']; ?></td>
				  	</tr>
				  	<tr>
				  		<td style="width: 25%">Descripcion</td>
				  		<td style="width: 75%"><?php echo $fila['prd_descripcion']; ?></td>
				  	</tr>
				  	<tr>
				  		<td style="width: 25%">Precio</td>
				  		<td style="width: 75%"><?php echo $fila['prd_precio']; ?></td>
				  	</tr>
				  	<tr>
				  		<td style="width: 25%">Miniatura</td>
				  		<td style="width: 75%"><img src="imagenes/<?php echo $fila['prd_foto1']; ?>" alt="" /></td>
				  	</tr>
				  	<tr>
				  		<td class="centrar" colspan="2">
				  			<input type="hidden" name="prd_id" value="<?php echo $prd_id; ?>" id="prd_id" />
				  			<input type="submit" name="baja" value="Confirmar baja" id="baja"/>
				  		</td>
				  	</tr>
				  </table>
				</div>
</div>
</form>		
	</div>
	<div id="pie">
		<?php  include "pie.php"  ?>
	</div>
	<script type="text/javascript" src="funciones.js">
	</script>
</body>
</html>
