<?php include "conexion.php"; ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Leer Base</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Precio</th>
			</tr>
		</thead>
		<?php while ($fila = mysqli_fetch_assoc($resultado)){

					?>
					<tr>
						<td><?php echo $fila['prd_nombre'];?> </td>
						<td><?php echo $fila['prd_descripcion'];?></td>
						<td>$ <?php echo $fila['prd_precio'];?></td>
					</tr>
					<?php
				} ?>
	</table>
	</div>
</body>
</html>
<?php mysqli_close($link); ?>