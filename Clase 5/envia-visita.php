<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Envia</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
   <div class="container">
    <form class="form-horizontal" role="form" action="procesa-visita.php" method="post">
      <div class="form-group">
        <label for="usuario" class="col-lg-2 control-label">Email</label>
        <div class="col-lg-10">
          <input type="text" class="form-control" id="usuario" name="usuario"
                 placeholder="Usuario">
        </div>
      </div>
      <div class="form-group">
        <label for="pass" class="col-lg-2 control-label">Comentarios</label>
        <div class="col-lg-10">
          <textarea name="coment" id="coment" cols="30" rows="10" class="form-control"></textarea>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10">
          <button type="submit" class="btn btn-default">Enviar</button>
        </div>
      </div>
    </form>
    </div>
</body>
</html>